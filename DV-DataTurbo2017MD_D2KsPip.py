def fillTuple( tuple, myBranches, myTriggerList ):

    from os import environ
    from GaudiKernel.SystemOfUnits import *
    from Gaudi.Configuration import *
    from Configurables import GaudiSequencer

    SeqPhys = GaudiSequencer("SeqPhys")

    tuple.Branches = myBranches
        
    tuple.ToolList = [
        "TupleToolAngles",
        "TupleToolEventInfo",
        "TupleToolGeometry",
        "TupleToolKinematic",
        "TupleToolPid",
        "TupleToolANNPID" ,
        "TupleToolPrimaries",
        "TupleToolPropertime",
        "TupleToolRecoStats",
        "TupleToolTrackInfo"
        ]

    # RecoStats for filling SpdMult, etc
    from Configurables import TupleToolRecoStats
    tuple.addTool(TupleToolRecoStats, name="TupleToolRecoStats")
    tuple.TupleToolRecoStats.Verbose=True

    from Configurables import TupleToolTISTOS, TupleToolDecay
    tuple.addTool(TupleToolDecay, name = 'D')
#    tuple.addTool(TupleToolDecay, name = 'KS')
    
    # TISTOS for Dp
    tistos = tuple.addTupleTool('TupleToolTISTOS')
    tistos.TriggerList = myTriggerList
    tistos.FillHlt2 = False
    tistos.Verbose = True
#    tuple.D.ToolList+=[ "TupleToolTISTOS" ]
#    tuple.D.addTool(TupleToolTISTOS, name="TupleToolTISTOS" )
#    tuple.D.TupleToolTISTOS.Verbose=True
#    tuple.D.TupleToolTISTOS.TriggerList = myTriggerList

    #LoKi one
    from Configurables import LoKi__Hybrid__TupleTool
    LoKi_All=LoKi__Hybrid__TupleTool("LoKi_All")
    LoKi_All.Variables = {
        "ETA"                  : "ETA",
        "Y"                    : "Y"  ,
        "LOKI_IPCHI2"          : "BPVIPCHI2()",
        "phi"		       : "PHI"
	}
    tuple.ToolList+=["LoKi::Hybrid::TupleTool/LoKi_All"]
    tuple.addTool(LoKi_All)
    fit = tuple.D.addTupleTool('TupleToolDecayTreeFitter/ReFit')
    fit.constrainToOriginVertex = True
    fit.Verbose= True
    fit.UpdateDaughters = True
    fit.daughtersToConstrain += ['KS0']

    
# DecayTreeTuple
from Configurables import DstConf, TurboConf # necessary for DaVinci v40r1 onwards
#TurboConf().PersistReco=True
#DstConf().Turbo=True

#from Configurables import DecayTreeTuple
from DecayTreeTuple.Configuration import *

myTriggerList = [
    # L0
    "L0ElectronDecision",
    "L0PhotonDecision",
    "L0HadronDecision",
    "L0MuonDecision",
    "L0MuonHighDecision",
    "L0DiMuonDecision",
    "L0PhysDecision",
    "L0GlobalDecision",
    
    # Hlt1 track    
    "Hlt1TrackAllL0Decision",
    "Hlt1TrackMuonDecision",
    "Hlt1TrackPhotonDecision",
    "Hlt1GlobalDecision",

    "Hlt1TrackMVADecision",
    "Hlt1TwoTrackMVADecision"
    ]


"""
"""
D2KsPip_Branch = {
    "KS"    :  "[ D+ -> ^(KS0 -> pi+ pi-)  pi+ ]CC",
    "KSpip"    :  "[ D+ -> (KS0 -> ^pi+ pi-)  pi+ ]CC",
    "KSpim"    :  "[ D+ -> (KS0 -> pi+ ^pi-)  pi+ ]CC",
    "Pip"    :  "[ D+ -> (KS0 -> pi+ pi-)  ^pi+ ]CC",
    "D"    :  "^([ D+ -> (KS0 -> pi+ pi-)  pi+ ]CC)"
}

the_line = "Hlt2CharmHadDp2KS0Pip_KS0DDTurbo/Particles"
D2KsPip_DD = DecayTreeTuple("D2KsPip_DD")
D2KsPip_DD.WriteP2PVRelations = False
D2KsPip_DD.RootInTES = "/Event/Charmcharged/Turbo"
D2KsPip_DD.Decay ="[ D+ -> ^(KS0 -> ^pi+ ^pi-)  ^pi+ ]CC"
D2KsPip_DD.Inputs = [ the_line ]
fillTuple( D2KsPip_DD,
           D2KsPip_Branch,
           myTriggerList )

the_line = "Hlt2CharmHadDp2KS0Pip_KS0LLTurbo/Particles"
D2KsPip_LL = DecayTreeTuple("D2KsPip_LL")
D2KsPip_LL.WriteP2PVRelations = False
D2KsPip_LL.RootInTES = "/Event/Charmcharged/Turbo"
D2KsPip_LL.Decay ="[ D+ -> ^(KS0 -> ^pi+ ^pi-)  ^pi+ ]CC"
D2KsPip_LL.Inputs = [ the_line ]
fillTuple( D2KsPip_LL,
           D2KsPip_Branch,
           myTriggerList )


"""
Options for building Stripping
"""
from Gaudi.Configuration import *
MessageSvc().Format = "% F%30W%S%7W%R%T %0W%M"

# Tighten Trk Chi2 to <3
from CommonParticles.Utils import DefaultTrackingCuts
DefaultTrackingCuts().Cuts  = { "Chi2Cut" : [ 0, 3 ],
                                "CloneDistCut" : [5000, 9e+99 ] }

#from PhysConf.Selections import MomentumScaling, AutomaticData, TupleSelection
#my_selection = AutomaticData ( the_line )
#my_selection = MomentumScaling ( my_selection, Turbo = True, Year = '2017' ) 

# create Selection sequence
#from PhysConf.Selections import SelectionSequence
#selseq = SelectionSequence ('MomScale', my_selection).sequence()

from Configurables import CondDB
CondDB ( LatestGlobalTagByDataType = '2017')

from Configurables import DaVinci
#DaVinci().appendToMainSequence( [ sc.sequence() ] )
DaVinci().InputType = 'MDST'                   # Addition for DaVinci v42r2 onwards 
DaVinci().RootInTES = '/Event/Charmcharged/Turbo'           # Addition for DaVinci v42r2 onwards 
DaVinci().Turbo = True                         # Addition for DaVinci v42r2 onwards
DaVinci().EvtMax =  -1                         # Number of events   -1
DaVinci().SkipEvents = 0                       # Events to skip
DaVinci().PrintFreq = 1000
DaVinci().DataType = "2017"
DaVinci().Simulation    = False
#DaVinci().DDDBtag   = "dddb-20150724"          # TEST
#DaVinci().CondDBtag = "cond-20170120-1"        # TEST
DaVinci().HistogramFile = "DVHistos.root"      # Histogram file
DaVinci().TupleFile = "Tuple.root"             # Ntuple
DaVinci().UserAlgorithms = [ ]
#DaVinci().UserAlgorithms += [ scaler ]
#DaVinci().UserAlgorithms += [ selseq ]
DaVinci().UserAlgorithms += [ D2KsPip_DD, D2KsPip_LL ]        # The algorithms
#DaVinci().Input = [ "/eos/lhcb/grid/prod/lhcb/LHCb/Collision16/CHARMCHARGED.MDST/00053792/0000/00053792_00009206_1.charmcharged.mdst"]   #commented
# MDST
#DaVinci().Input = [ "00066595_00008698_1.charmcharged.mdst" ]


# Get Luminosity
DaVinci().Lumi = True
DaVinci().InputType = "MDST"

#lb-run davinci/v42r6p1 DV-DataTurbo2017_D2KsPip.py
