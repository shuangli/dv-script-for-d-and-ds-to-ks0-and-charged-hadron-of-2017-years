myJobName = 'D2KsPip_2017_MagDown_Turbo03a'
myApplication = GaudiExec()  #'DaVinci', 'v42r7p3'
myApplication.directory = '~/davinci/DaVinciDev_v44r10p5'
myApplication.platform='x86_64-slc6-gcc62-opt'
myApplication.options = [ 'DV-DataTurbo2017MD_D2KsPip.py']


data = BKQuery('/LHCb/Collision17/Beam6500GeV-VeloClosed-MagDown/Real Data/Turbo04/94000000/CHARMCHARGED.MDST',
                       dqflag=['OK']).getDataset()

mySplitter = SplitByFiles( filesPerJob = 30, maxFiles = -1, ignoremissing = True, bulksubmit=False )
myBackend = Dirac()
j = Job (        
    name         = myJobName,
    application  = myApplication,
    splitter     = mySplitter,
#    outputfiles  = [ DiracFile('Tuple.root')],
    outputfiles  = [ LocalFile('Tuple.root')],
    backend      = myBackend,
    inputdata    = data,
    do_auto_resubmit =False 
    )
j.submit(keep_going=True, keep_on_fail=True)
